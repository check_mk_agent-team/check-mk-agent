#!/bin/bash
shopt -s extglob
[ "${PWD##*/}" != "check-mk-agent" ] && echo run this script from check-mk-agent directory
OLDVERSION=$(dpkg-parsechangelog -S Version)
UPSTREAMVERSION=$(uscan --dehs --no-download --no-verbose |xml2 |grep upstream-version |sed 's|^/dehs/upstream-version=||g')
cd ../check-mk-agent-dfsg || echo You should have ../check-mk-agent-dfsg directory || exit 2
git stash
git rebase --abort
git merge --abort
git restore .
git clean -fxd
git fetch github
git checkout -b dfsg_clean v${UPSTREAMVERSION}
git rm -rf !(debian|agents|COPYING|AUTHORS|defines.make|.git)
TOREMOVE=$(echo .!(git|gitignore|gitmodules) |sed 's/\. \.\. / /g')
git rm -rf $TOREMOVE agents/wnx agents/windows
TOREMOVE=$(find -name '*.solaris' -or -name '*.aix' -or -name '*.openbsd' -or -name '*.openwrt' -or -name '*.openvms' -or -name '*.freebsd' -or -name '*.macosx' -or -name '*.netbsd' -or -name '*.hpux')
git rm -rf $TOREMOVE
git rm -rf \
agents/wnx \
agents/windows \
agents/plugins/mk_suseconnect \
agents/plugins/db2_mem \
agents/plugins/mk_saprouter \
agents/plugins/mk_zypper \
agents/plugins/mk_sap_hana \
agents/plugins/mk_oracle_crs \
agents/plugins/unitrends_replication.py \
agents/plugins/hpux_statgrab \
agents/plugins/mk_oracle \
agents/plugins/vxvm \
agents/plugins/mk_db2.linux \
agents/plugins/mk_sap.py \
agents/plugins/websphere_mq \
agents/plugins/mk_informix \
agents/plugins/hpux_lunstats

git rm -rf \
agents/modules \
agents/z_os \
agents/special \
agents/waitmax \
agents/sap \
agents/__init__.py \
agents/.f12 \
agents/cfg_examples/sqlplus.sh \
agents/.gitignore \
agents/CONTENTS \
agents/cfg_examples/mk_oracle.cfg \
agents/cfg_examples/saprouter.cfg \
agents/cfg_examples/sqlnet.ora \
agents/cfg_examples/tnsnames.ora \
agents/check-mk-agent.spec \
agents/plugins/.f12 \
agents/plugins/.gitignore \
agents/plugins/Makefile \
agents/plugins/mk_tsm \
agents/plugins/zorp \
agents/plugins/unitrends_backup

git rm -rf \
agents/debian \
agents/plugins/__init__.py \
agents/scripts/cmk-agent-useradd.sh \
agents/plugins/mk_sap_2.py \
agents/scripts/super-server/setup \
agents/scripts/super-server/0_systemd/setup \
agents/scripts/super-server/0_systemd/check-mk-agent.socket \
agents/scripts/super-server/0_systemd/cmk-agent-ctl-daemon.service \
agents/scripts/super-server/1_xinetd/setup

git rm -rf agents/conffiles/super-server.cfg .gitignore .gitmodules
git update-index --chmod=-x agents/scripts/super-server/1_xinetd/check-mk-agent
git add --chmod=-x agents/scripts/super-server/1_xinetd/check-mk-agent
chmod -x agents/scripts/super-server/1_xinetd/check-mk-agent
git diff v${OLDVERSION/-?/} --numstat
while [[ "y" != "$MYTEST" ]] ; do read -n1 -p "Type y if changes match changelog:" MYTEST ; [[ "y" == "$MYTEST" ]] && git commit -m "dfsg clean v$UPSTREAMVERSION" && git tag "v$UPSTREAMVERSION+dfsg" -m "dfsg clean v$UPSTREAMVERSION" && git diff v${OLDVERSION/-?/}..v$UPSTREAMVERSION+dfsg --compact-summary ; echo; done
git checkout dfsg_clean_merge
git merge -s ort -X theirs "v$UPSTREAMVERSION+dfsg"
git push --follow-tags
cd ../check-mk-agent
git stash
git rebase --abort
git merge --abort
git restore .
git clean -fxd
git fetch salsa
git checkout debian/latest
gbp dch --distribution=unstable -a -N ${UPSTREAMVERSION}+dfsg-1
git commit -a -m "New upstream release v$UPSTREAMVERSION+dfsg-1"
gbp tag --retag
git commit -a -m "New upstream release v$UPSTREAMVERSION+dfsg-1"
git branch -D dfsg_clean
git push --follow-tags
