check-mk-agent (2.3.0p27+dfsg-2) unstable; urgency=medium

  * New upstream release v2.3.0p27+dfsg

 -- Juri Grabowski <debian@jugra.de>  Thu, 20 Feb 2025 16:09:25 +0100

check-mk-agent (2.3.0p25+dfsg-1) unstable; urgency=medium

  * New upstream release v2.3.0p25+dfsg-1

 -- Juri Grabowski <debian@jugra.de>  Thu, 30 Jan 2025 15:06:07 +0100

check-mk-agent (2.3.0p22+dfsg-1) unstable; urgency=medium

  * New upstream release v2.3.0p21+dfsg-1
  * add alternative dependencies

 -- Juri Grabowski <debian@jugra.de>  Tue, 10 Dec 2024 11:01:43 +0100

check-mk-agent (2.3.0p21+dfsg-1) unstable; urgency=medium

  * New upstream release v2.3.0p21+dfsg-1

 -- Juri Grabowski <debian@jugra.de>  Tue, 26 Nov 2024 10:11:52 +0100

check-mk-agent (2.3.0p19+dfsg-1) unstable; urgency=medium

  * let agents here
  * add new url
  * Update to new windows agent structure

 -- Juri Grabowski <debian@jugra.de>  Mon, 04 Nov 2024 10:47:11 +0100

check-mk-agent (2.2.0p24+dfsg-1) unstable; urgency=medium

  * New upstream release v2.2.0p24+dfsg-1

 -- Juri Grabowski <debian@jugra.de>  Sun, 07 Apr 2024 19:02:32 +0200

check-mk-agent (2.1.0p41+dfsg-1) unstable; urgency=medium

  * New upstream release v2.1.0p41

 -- Juri Grabowski <debian@jugra.de>  Sun, 07 Apr 2024 17:13:35 +0200

check-mk-agent (2.1.0p18+dfsg-1) unstable; urgency=medium

  * New upstream release v2.1.0p18

 -- Juri Grabowski <debian@jugra.de>  Mon, 19 Dec 2022 12:19:21 +0100

check-mk-agent (2.1.0p17+dfsg-1) unstable; urgency=medium

  * Remove applied upstream Documentation-URL.patch

 -- Juri Grabowski <debian@jugra.de>  Wed, 30 Nov 2022 10:14:06 +0100

check-mk-agent (2.1.0p16+dfsg-1) unstable; urgency=medium

  * New upstream release v2.1.0p16

 -- Juri Grabowski <debian@jugra.de>  Sun, 27 Nov 2022 20:36:11 +0100

check-mk-agent (2.1.0p15+dfsg-1) unstable; urgency=medium

  * Update to new upstream version 2.1.0p15

 -- Juri Grabowski <debian@jugra.de>  Wed, 26 Oct 2022 11:19:29 +0200

check-mk-agent (2.1.0p13+dfsg-2) unstable; urgency=medium

  * Fix lintian: very-long-line-length-in-source-file 609 > 512 [debian/check_mk_agent.1:72] (override comment: P: check-mk-agent source: very-long-line-length-in-source-file agents/plugins/mk_tsm line 62 is 700 characters long (>512) It's just a comment)
  * Update SV to 4.6.1, no changes needed
  * remove components, which depends on non-free software, to make it possible to upload to main
  * Trim trailing whitespace.
  * Set debhelper-compat version in Build-Depends.
  * Re-export upstream signing key without extra signatures.
  * Avoid explicitly specifying -Wl,--as-needed linker flag.

 -- Juri Grabowski <debian@jugra.de>  Sat, 01 Oct 2022 15:15:13 -0000

check-mk-agent (2.1.0p13+dfsg-1) unstable; urgency=medium

  * move to check_mk_agent-team
  * compare to dfsg oldversion

 -- Juri Grabowski <debian@jugra.de>  Sat, 01 Oct 2022 14:11:37 +0200

check-mk-agent (2.1.0p12+dfsg-1) unstable; urgency=medium

  * address ftp-master issue about GPL-2.0+
  * Update to new upstream version 2.1.0p12

 -- Juri Grabowski <debian@jugra.de>  Sat, 24 Sep 2022 16:41:23 +0200

check-mk-agent (2.1.0p4+dfsg-1) unstable; urgency=medium

  * Initial release (Closes: #998452) general purpose monitoring plugin

 -- Juri Grabowski <debian@jugra.de>  Wed, 08 Jun 2022 16:34:26 +0200
